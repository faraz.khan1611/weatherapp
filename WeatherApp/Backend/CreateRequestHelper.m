//
//  CreateRequestHelper.m
//  WeatherApp
//
//  Created by Faraz Khan on 3/2/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

#import "CreateRequestHelper.h"

@implementation CreateRequestHelper


+(NSMutableURLRequest*)createRequestForOpenWeatherAPI:(NSString*)apiURL andLat:(double)lat andLong:(double)longitude
{
    NSString *url = [[NSString alloc] initWithFormat:apiURL,lat,longitude,API_KEY];
    NSURL *requestUrl = [NSURL URLWithString:url];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestUrl];
    [request setHTTPMethod:@"GET"];
    [request addValue:@"application/json; charset=UTF-8" forHTTPHeaderField: @"Content-Type"];
    return request;
}

+(NSMutableURLRequest*)createRequestForIconWithOpenWeatherAPI:(NSString*)apiURL andIconID:(NSString*)iconId
{
    NSURL *requestUrl = [NSURL URLWithString:[[NSString alloc] initWithFormat:apiURL,iconId]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestUrl];
//    [request setHTTPMethod:@"GET"];
//    [request addValue:@"image/jpeg" forHTTPHeaderField: @"Content-Type"];
    return request;
}
@end
