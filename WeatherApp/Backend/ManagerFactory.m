//
//  ManagerFactory.m
//  WeatherApp
//
//  Created by Faraz Khan on 3/1/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

#import "ManagerFactory.h"

@implementation ManagerFactory

-(LocationManager*)getLocationManagerWithDelegate:(id<LocationInfoChangeDelegate>)delegate
{
    return [[LocationManager alloc] initWithDelegate:delegate];
}
-(OpenWeatherManager*)getWeatherManagerinitWithDelegate:(id<WeatherInfoChangeDelegate>)delegate
{
    return [[OpenWeatherManager alloc] initWithDelegate:delegate];
}
-(HttpManager*)getHttpManagerWithDelegate:(id<HttpManagerDelegate>)delegate
{
    return [[HttpManager alloc] initWithDelegate:delegate];
}

@end
