//
//  CreateRequestHelper.h
//  WeatherApp
//
//  Created by Faraz Khan on 3/2/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CreateRequestHelper : NSObject
+(NSMutableURLRequest*)createRequestForOpenWeatherAPI:(NSString*)apiURL andLat:(double)lat andLong:(double)longitude;
+(NSMutableURLRequest*)createRequestForIconWithOpenWeatherAPI:(NSString*)apiURL andIconID:(NSString*)iconId;
@end
