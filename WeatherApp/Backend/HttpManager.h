//
//  HttpManager.h
//  WeatherApp
//
//  Created by Faraz Khan on 3/1/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//USED to make network calls and return to weather manager

#import <Foundation/Foundation.h>

@protocol HttpManagerDelegate<NSObject>
- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data;
@end

@interface HttpManager : NSObject

-(HttpManager*)initWithDelegate:(id<HttpManagerDelegate>)delegate;
-(void)executeURLRequest:(NSURLRequest*)request;
-(void)tearDown;
@end
