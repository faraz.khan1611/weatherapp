//
//  WController.m
//  WeatherApp
//
//  Created by Faraz Khan on 3/1/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

#import "WController.h"
#import "ManagerFactory.h"
#import "HttpManager.h"
#import "LocationInfoObject.h"
#import "LocationManager.h"
#import "OpenWeatherManager.h"

@interface WController()<WeatherInfoChangeDelegate,LocationInfoChangeDelegate>
@property (nonatomic) ManagerFactory *factory;
@property (nonatomic) HttpManager *httpManager;
@property (nonatomic) NSDate *lastWeatherUpdateTime;
@property (nonatomic) NSTimer *timer;
@property (nonatomic) LocationInfoObject *currentLocationInfo;
@property (nonatomic) WeatherInfoObject *currentWeatherInfo;
@property (nonatomic) NSData *currentIconForWeather;
@property (nonatomic) NSHashTable *listeners;
@property (nonatomic) OpenWeatherManager *weatherManager;
@property (nonatomic) LocationManager *locationManager;
@end

@implementation WController

static WController *sharedWCInt = nil;

+ (id)sharedInstance {
    
    if(sharedWCInt == nil)
    {
        sharedWCInt = [[self alloc] init];
    }
    
    return sharedWCInt;
}

- (id)init
{
    self = [super init];
    if( self)
    {
        self.factory = [[ManagerFactory alloc] init];
        self.listeners = [NSHashTable weakObjectsHashTable];
    }
    
    return self;
}

-(HttpManager*)httpManager
{
    if(_httpManager == nil)
    {
        self.httpManager = [self.factory getHttpManagerWithDelegate:self.weatherManager];
    }
    return _httpManager;
}

-(LocationManager*)locationManager
{
    if(_locationManager == nil)
    {
        self.locationManager = [self.factory getLocationManagerWithDelegate:self];
    }
    return _locationManager;
}

-(OpenWeatherManager*)weatherManager
{
    if(_weatherManager == nil)
    {
        self.weatherManager = [self.factory getWeatherManagerinitWithDelegate:self];
        [self httpManager];
        [_weatherManager setHTTPManager:self.httpManager];
    }
    return _weatherManager;
}

-(WeatherInfoObject*)getCurrentWeatherInfo
{
    return self.currentWeatherInfo;
}

-(LocationPermissionState)getCurrentLocationPermissionState
{
    return [self.locationManager permissionState];
}

-(NSData*)getCurrentWeatherIcon
{
    return self.currentIconForWeather;
}

-(void)enableLocation
{
    NSLog(@"Wcontroller enableLocation");
    if(self.currentLocationInfo != nil)
    {
        [self scheduleTimer];
    }
    [[self locationManager] enableLocation];
}

-(void)disableLocation
{
    NSLog(@"Wcontroller disableLocation");
    [self cancelTimer];
    [[self locationManager] disableLocation];
}

//Logic refereshes weather after every one hour, configurable by constants file
-(void)scheduleTimer
{
    NSLog(@"Wcontroller scheduleTimer");
    if(self.timer !=nil)
    {
        [self.timer invalidate];
        self.timer = nil;
    }
    self.timer = [NSTimer timerWithTimeInterval:WEATHER_REFERESH_INTERVAL_SECS repeats:NO block:^(NSTimer * _Nonnull timer) {
        NSLog(@"Wcontroller TImer Fired");
        if(self.currentLocationInfo)
        {
            [self.weatherManager getCurrentWeatherForLat:self.currentLocationInfo.latitude Long:self.currentLocationInfo.longitude];
        }
    }];
    NSRunLoop *runLoop = [NSRunLoop mainRunLoop];
    [runLoop addTimer:self.timer forMode:NSDefaultRunLoopMode];
}

-(void)cancelTimer
{
    NSLog(@"Wcontroller cancel Timer");
    if(self.timer != nil)
    {
        [self.timer invalidate];
        self.timer = nil;
    }
}

-(void)tearDown
{
    [self cancelTimer];
    [self.locationManager tearDown];
    [self.httpManager tearDown];
    [self.weatherManager tearDown];
}
// Add a listener
- (void)addListener:(id<WControllerDelegate>)listener
{
    @synchronized (self.listeners)
    {
        [self.listeners addObject:listener];
    }
}

// Remove a listener
- (void)removeListener:(id<WControllerDelegate>)listener
{
    @synchronized (self.listeners)
    {
        if([self.listeners containsObject:listener])
            [self.listeners removeObject:listener];
    }
}

//-(BOOL)checkIfWeatherUpdateRequired {
//    if(self.lastWeatherUpdateTime == nil || [self.lastWeatherUpdateTime timeIntervalSinceNow] > WEATHER_REFERESH_INTERVAL_SECS)
//    {
//        NSLog(@"checkIfWeatherUpdateRequired return YES");
//        return YES;
//    }
//    else if(self.currentLocationInfo == nil)
//    {
//        NSLog(@"checkIfWeatherUpdateRequired self.currentLocationInfo == nil return YES");
//        return YES;
//    }
//    else if(self.currentWeatherInfo == nil)
//    {
//        NSLog(@"checkIfWeatherUpdateRequired self.currentWeatherInfo == nil return YES");
//        return YES;
//    }
//    else
//    {
//        NSLog(@"checkIfWeatherUpdateRequired return NO");
//        return NO;
//    }
//}


#pragma WeatherInfoDelegate
- (void)weatherInfoError {
    NSLog(@"WController weatherInfoError");
    self.currentWeatherInfo = nil;
    self.currentIconForWeather = nil;
    [self cancelTimer];
    [self scheduleTimer];
    @synchronized (self.listeners)
    {
        for (__weak id<WControllerDelegate> listener in self.listeners)
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                [listener weatherInfoError];
                
            });
        }
    }
}

- (void)weatherInfoUpdatedWith:(WeatherInfoObject *)info
{
    NSLog(@"WController weatherInfoUpdatedWith");
    self.lastWeatherUpdateTime = [NSDate date];
    self.currentWeatherInfo = info;
    self.currentIconForWeather = nil; // set it as nil as old icon may no longer be valid and new request need to be made.
    [self cancelTimer];
    [self scheduleTimer];
    @synchronized (self.listeners)
    {
        for (__weak id<WControllerDelegate> listener in self.listeners)
        {
             [listener weatherInfoUpdatedWith:info];
        }
    }
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self.weatherManager getCurrentWeatherIconID:info.iconID];
    });
}

-(void) weatherIconUpdated:(NSData*)data
{
    NSLog(@"WController weatherIconUpdated");
    self.currentIconForWeather = data;
    @synchronized (self.listeners)
    {
        for (__weak id<WControllerDelegate> listener in self.listeners)
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                [listener weatherIconUpdated:data];
                
            });
        }
    }
}

#pragma LocationInfoDelegate
- (void)locationInfoError {
    NSLog(@"WController locationInfoError");
    self.currentLocationInfo = nil;
    @synchronized (self.listeners)
    {
        for (__weak id<WControllerDelegate> listener in self.listeners)
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                [listener locationInfoError];
                
            });
        }
    }
}

- (void)locationInfoUpdatedWithLat:(double)lat andLong:(double)longitute {
    NSLog(@"%@", [NSString stringWithFormat:@"WController locationInfoUpdatedWithLat %f long %f",lat,longitute]);
    self.currentLocationInfo = [[LocationInfoObject alloc] initWithLat:lat main:longitute];
    //After informing openWeatherManager , inform all VM's to handle
    
    @synchronized (self.listeners)
    {
        for (__weak id<WControllerDelegate> listener in self.listeners)
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                [listener locationInfoUpdatedWithLat:lat andLong:longitute];
                
            });
        }
    }
    //As new val will be calculated, this will be null
    self.currentWeatherInfo = nil;
    self.currentIconForWeather = nil;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSLog(@"WController getCurrentWeatherForLat fired");
        [self.weatherManager getCurrentWeatherForLat:lat Long:longitute];
    });
}

- (void)locationPermisionChange:(LocationPermissionState)state {
    NSLog(@"WController locationPermisionChange");
    @synchronized (self.listeners)
    {
        for (__weak id<WControllerDelegate> listener in self.listeners)
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                [listener locationPermissionChanged:state];
                
            });
        }
    }
    
}

@end
