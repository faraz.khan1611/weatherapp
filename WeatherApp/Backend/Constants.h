//
//  Constants.h
//  WeatherApp
//
//  Created by Faraz Khan on 3/2/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, LocationPermissionState)
{
    LocationPermissionStateAuthorizationStatusNotDetermined,
    LocationPermissionStateAuthorizationStatusDenied,
    LocationPermissionStateLocationAuthorized
};
static NSString* const OPEN_WEATHER_API_BASE_URL = @"http://api.openweathermap.org/data/2.5/weather?lat=%f&lon=%f&appid=%@";
static NSString* const OPEN_WEATHER_ICON_API_BASE_URL = @"http://openweathermap.org/img/w/%@.png";
static NSString* const API_KEY = @"b109583d8f60e1d6dd7c9bdd224e7c8a";
static const int WEATHER_REFERESH_INTERVAL_SECS = 60 * 30;
static const int LOCATION_DIFF_IN_METERS = 100; // more the hundred meter , referesh weather



@interface Constants : NSObject

@end
