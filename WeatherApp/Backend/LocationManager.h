//
//  LocationManager.h
//  WeatherApp
//
//  Created by Faraz Khan on 3/1/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol LocationInfoChangeDelegate<NSObject>

- (void)locationInfoUpdatedWithLat:(double)lat andLong:(double)longitute;
-(void)locationInfoError;
-(void)locationPermisionChange:(LocationPermissionState)state;

@end

@interface LocationManager : NSObject

@property LocationPermissionState permissionState;
@property CLLocationManager* locationManager;
-(LocationManager*)initWithDelegate:(id<LocationInfoChangeDelegate>)delegate;
-(BOOL) enableLocation;
-(BOOL) disableLocation;
-(LocationPermissionState) updateLocationPermission;
-(void)tearDown;
@end
