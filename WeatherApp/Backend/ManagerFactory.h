//
//  ManagerFactory.h
//  WeatherApp
//
//  Created by Faraz Khan on 3/1/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocationManager.h"
#import "OpenWeatherManager.h"
#import "HttpManager.h"

@interface ManagerFactory : NSObject

-(LocationManager*)getLocationManagerWithDelegate:(id<LocationInfoChangeDelegate>)delegate;
-(OpenWeatherManager*)getWeatherManagerinitWithDelegate:(id<WeatherInfoChangeDelegate>)delegate;
-(HttpManager*)getHttpManagerWithDelegate:(id<HttpManagerDelegate>)delegate;
@end
