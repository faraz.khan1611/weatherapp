//
//  LocationManager.m
//  WeatherApp
//
//  Created by Faraz Khan on 3/1/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

#import "LocationManager.h"

@interface LocationManager()<CLLocationManagerDelegate>
@property(nonatomic, weak) id<LocationInfoChangeDelegate> delegate;
@property(nonatomic) CLLocation *lastCordinates;
@end

@implementation LocationManager

-(LocationManager*)initWithDelegate:(id<LocationInfoChangeDelegate>)delegate
{
    self = [self init];
    
    if(self)
    {
        self.delegate = delegate;
    }
    return self;
}

-(void)tearDown
{
    [self disableLocation];
    self.locationManager = nil;
}


-(BOOL) enableLocation;
{
    CLAuthorizationStatus authorizationStatus= [CLLocationManager authorizationStatus];
    switch (authorizationStatus) {
        case kCLAuthorizationStatusNotDetermined:
            if(!self.locationManager){
                self.locationManager = [[CLLocationManager alloc] init];
                self.locationManager.delegate = self;
                
            }
            
            [self.locationManager requestAlwaysAuthorization];
            
            [self setPermission:LocationPermissionStateAuthorizationStatusNotDetermined];
            return false;
            
        case kCLAuthorizationStatusAuthorizedAlways:
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            
            
            if(!self.locationManager){
                self.locationManager = [[CLLocationManager alloc] init];
                self.locationManager.delegate = self;
                
            }
            
            if([CLLocationManager significantLocationChangeMonitoringAvailable]){
                [self setPermission:LocationPermissionStateLocationAuthorized];
                [self.locationManager startMonitoringSignificantLocationChanges];
                return true;
            }else{
            }
            
        default:
            [self setPermission:LocationPermissionStateAuthorizationStatusDenied];
            return false;
    }
}

-(BOOL) disableLocation
{
    if(self.locationManager)
    {
        [self.locationManager stopMonitoringSignificantLocationChanges];
        return YES;
    }
    return YES;
}

-(LocationPermissionState) updateLocationPermission
{
    CLAuthorizationStatus authorizationStatus= [CLLocationManager authorizationStatus];
    
    switch (authorizationStatus) {
        case kCLAuthorizationStatusDenied:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            [self setPermission:LocationPermissionStateAuthorizationStatusDenied];
            return LocationPermissionStateAuthorizationStatusDenied;
            
        case LocationPermissionStateAuthorizationStatusNotDetermined:
            [self setPermission:LocationPermissionStateAuthorizationStatusNotDetermined];
            return LocationPermissionStateAuthorizationStatusNotDetermined;
            
        default:
            [self setPermission:LocationPermissionStateLocationAuthorized];
            return LocationPermissionStateLocationAuthorized;
    }
}

-(void) setPermission:(LocationPermissionState) permissionState
{
    if([self.delegate respondsToSelector:@selector(locationPermisionChange:)])
    {
        self.permissionState = permissionState;
        [self.delegate locationPermisionChange:permissionState];
    }
}

-(BOOL)checkIfLocationDiffFromLastLocationIsMore:(CLLocation*)loc
{
    if(self.lastCordinates == nil)
    {
        NSLog(@"checkIfLocationDiffFromLastLocationIsMore self.lastCordinates == nil return YES");
        return YES;
    }
    CLLocationDistance diff = [loc distanceFromLocation:self.lastCordinates];
    if(diff > LOCATION_DIFF_IN_METERS)
    {
        NSLog(@"%@", [NSString stringWithFormat:@"checkIfLocationDiffFromLastLocationIsMore diff %f > LOCATION_DIFF_IN_METERS return YES",diff]);
        return YES;
    }
    else
    {
        NSLog(@"checkIfLocationDiffFromLastLocationIsMore return No");
        self.lastCordinates = loc;
        return NO;
    }
}

#pragma mark Core Location delgates
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    NSLog(@"Location Changed");
    if([self.delegate respondsToSelector:@selector(locationInfoUpdatedWithLat:andLong:)])
    {
        for(int i=0;i<locations.count;)
        {
            if([self checkIfLocationDiffFromLastLocationIsMore:locations[i]])
            {
                
                self.lastCordinates = locations[i];
                [self.delegate locationInfoUpdatedWithLat:self.lastCordinates.coordinate.latitude andLong:self.lastCordinates.coordinate.longitude];
                break;
            }
        }
        
        
    }
}


- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"Location didFailWithError");
    CLAuthorizationStatus authorizationStatus= [CLLocationManager authorizationStatus];
    
    switch (authorizationStatus) {
        case kCLAuthorizationStatusDenied:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            [self setPermission:LocationPermissionStateAuthorizationStatusDenied];
            break;
            
        default:
            break;
            
    }
    
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    
    NSLog(@"Location didChangeAuthorizationStatus");
    [self updateLocationPermission];
}
@end
