//
//  WController.h
//  WeatherApp
//
//  Created by Faraz Khan on 3/1/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WeatherInfoObject.h"
@protocol WControllerDelegate
@optional
-(void) weatherInfoUpdatedWith:(WeatherInfoObject*)info;
-(void) weatherIconUpdated:(NSData*)data;
-(void) weatherInfoError;
-(void)locationInfoUpdatedWithLat:(double)lat andLong:(double)longitute;
-(void) locationInfoError;
-(void)locationPermissionChanged:(LocationPermissionState)state;
@end

@interface WController : NSObject
+(id)sharedInstance;
-(LocationPermissionState)getCurrentLocationPermissionState;
-(void)scheduleTimer;
-(void)enableLocation;
-(void)disableLocation;
-(WeatherInfoObject*)getCurrentWeatherInfo;
-(NSData*)getCurrentWeatherIcon;
-(void)addListener:(id<WControllerDelegate>)listener;
-(void)removeListener:(id<WControllerDelegate>)listener;
-(void)cancelTimer;
-(void)tearDown;
@end
