//
//  OpenWeatherManager.h
//  WeatherApp
//
//  Created by Faraz Khan on 3/1/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HttpManager.h"
#import "WeatherInfoObject.h"

@protocol WeatherInfoChangeDelegate<NSObject>

-(void)weatherInfoUpdatedWith:(WeatherInfoObject*)info;
-(void)weatherInfoError;
-(void)weatherIconUpdated:(NSData*)data;

@end

@interface OpenWeatherManager : NSObject<HttpManagerDelegate>

-(OpenWeatherManager*)initWithDelegate:(id<WeatherInfoChangeDelegate>)delegate;
-(void)getCurrentWeatherForLat:(double)latitude Long:(double) longitude;
-(void)setHTTPManager:(HttpManager*)httpManager;
-(void)getCurrentWeatherIconID:(NSString*)iconID;
-(void)tearDown;
@end
