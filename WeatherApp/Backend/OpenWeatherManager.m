//
//  OpenWeatherManager.m
//  WeatherApp
//
//  Created by Faraz Khan on 3/1/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

#import "OpenWeatherManager.h"
#import "WController.h"
#import "CreateRequestHelper.h"

@interface OpenWeatherManager()
@property(nonatomic, weak) id<WeatherInfoChangeDelegate> delegate;
@property(nonatomic) HttpManager *httpManager;
@end

@implementation OpenWeatherManager

-(OpenWeatherManager*)initWithDelegate:(id<WeatherInfoChangeDelegate>)delegate
{
    self = [self init];
    
    if(self)
    {
        self.delegate = delegate;
    }
    return self;
}


-(void)setHTTPManager:(HttpManager*)httpManager
{
    self.httpManager = httpManager;
}

-(void)getCurrentWeatherForLat:(double)latitude Long:(double) longitude
{
    if(self.httpManager)
    {
        NSURLRequest *request = [CreateRequestHelper createRequestForOpenWeatherAPI:OPEN_WEATHER_API_BASE_URL andLat:latitude andLong:longitude];
        [self.httpManager executeURLRequest:request];
    }
}

-(void)getCurrentWeatherIconID:(NSString*)iconID
{
    if(self.httpManager)
    {
        NSURLRequest *request = [CreateRequestHelper createRequestForIconWithOpenWeatherAPI:OPEN_WEATHER_ICON_API_BASE_URL andIconID:iconID];
        [self.httpManager executeURLRequest:request];
    }
}

-(void)tearDown
{
    [self.httpManager tearDown];
}

#pragma HttpManager Delegate
- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data {
        
    if(self.delegate)
    {
        if([dataTask error] == nil)
        {
            if([dataTask.currentRequest.URL.absoluteString containsString:@"data"])
            {
                if([self.delegate respondsToSelector:@selector(weatherInfoUpdatedWith:)])
                {
                    NSError *error = nil;
                    WeatherInfoObject *info;
                    @try {
                        NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:data
                                                                               options:kNilOptions
                                                                                 error:&error];
                        info = [[WeatherInfoObject alloc] initWithDict:dict];
                    }
                    @catch (NSException * e) {
                        NSLog(@"Exception: %@", e);
                    }
                    [self.delegate weatherInfoUpdatedWith:info];
                }
            }
            else
            {
                if([self.delegate respondsToSelector:@selector(weatherIconUpdated:)])
                {
                    [self.delegate weatherIconUpdated:data];
                }
            }
        }
        else
        {
            if([self.delegate respondsToSelector:@selector(weatherInfoError)])
            {
                [self.delegate weatherInfoError];
            }
        }
    }
    
}

@end
