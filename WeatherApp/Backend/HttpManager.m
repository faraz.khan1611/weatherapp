//
//  HttpManager.m
//  WeatherApp
//
//  Created by Faraz Khan on 3/1/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

#import "HttpManager.h"
@interface HttpManager()<NSURLSessionDelegate>
@property(nonatomic,weak) id<HttpManagerDelegate> delegate;
@property(strong, nonatomic) NSURLSession* session;
@end

@implementation HttpManager

-(HttpManager*)initWithDelegate:(id<HttpManagerDelegate>)delegate
{
    self = [super init];
    if(self != nil)
    {
        self.delegate = delegate;
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        configuration.discretionary = NO;
        configuration.HTTPMaximumConnectionsPerHost = 1;
        [configuration setAllowsCellularAccess:YES];
        configuration.HTTPAdditionalHeaders = @{
                                                @"Accept-Language" : @"en",
                                                };
        self.session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    }
    return self;
}

- (void) tearDown{
    if(self.session){
        [self.session invalidateAndCancel];
        self.session = nil;
    }
}

-(void)executeURLRequest:(NSURLRequest*)request
{
    [[self.session dataTaskWithRequest:request] resume];
}

#pragma mark -- NSURLSessionDataDelegate
- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data
{
    [self.delegate URLSession:session dataTask:dataTask didReceiveData:data];
}
@end
