//
//  AppInteractor.m
//  WeatherApp
//
//  Created by Faraz Khan on 3/1/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
// THIS CLASS LINKS APP TO BACK END CODE

#import "AppInteractor.h"

@implementation AppInteractor

static AppInteractor *sharedAppInt = nil;

+ (id)sharedInstance {
    
    if(sharedAppInt == nil)
    {
        sharedAppInt = [[self alloc] initWithWController];
    }
    
    return sharedAppInt;
}

-(AppInteractor*)initWithWController
{
    self = [self init];
    
    if (self)
    {
        self.wctrl = [WController sharedInstance];
    }
    
    return self;
}

- (WeatherVM*)getWeatherVm:(id<WeatherVMDelegate>)delegate
{
    return [[WeatherVM alloc] init:self.wctrl delegate:delegate];
}

@end
