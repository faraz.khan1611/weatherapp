//
//  AppInteractor.h
//  WeatherApp
//
//  Created by Faraz Khan on 3/1/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WeatherVM.h"
#import "WController.h"

@interface AppInteractor : NSObject

@property(nonatomic) WController *wctrl;

+ (id)sharedInstance;
- (WeatherVM*)getWeatherVm:(id<WeatherVMDelegate>)delegate;
@end
