//
//  WeatherVM.h
//  WeatherApp
//
//  Created by Faraz Khan on 3/1/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocationManager.h"
#import "WeatherInfoObject.h"
#import "WController.h"

@protocol WeatherVMDelegate<NSObject>
-(void) weatherInfoUpdated;
-(void) weatherIconUpdated:(NSData*)data;
-(void) weatherInfoError;
-(void)locationInfoUpdatedWithLat:(double)lat andLong:(double)longitute;
-(void) locationInfoError;
- (void)locationPermissionChanged:(LocationPermissionState)state;
@end
//For time being using callback, later should be using RXSwift instead
@interface WeatherVM : NSObject<WControllerDelegate>

-(WeatherVM*)init:(WController*)cntrl delegate:(id<WeatherVMDelegate>)delegate;
-(WeatherInfoObject*)getCurrentWeatherInfo;
-(NSData *)getCurrentWeatherIcon;
-(void)tearDown;
@end
