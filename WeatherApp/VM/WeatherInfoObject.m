//
//  WeatherInfoObject.m
//  WeatherApp
//
//  Created by Faraz Khan on 3/1/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

#import "WeatherInfoObject.h"

@implementation WeatherInfoObject

-(WeatherInfoObject*)init:(int)ID main:(NSString*)main description:(NSString*)desc iconID:(NSString*)iconID temprature:(float)temprature country:(NSString*)country city:(NSString*)city
{
    self = [super init];
    if (self)
    {
        self.ID = ID;
        self.main = main;
        self.desc = desc;
        self.iconID = iconID;
        self.temprature = temprature;
        self.country = country;
        self.city = city;
    }
    return self;
}

-(WeatherInfoObject*)initWithDict:(NSDictionary*)dict
{
//    {
//        base = stations;
//        clouds =     {
//            all = 90;
//        };
//        cod = 200;
//        coord =     {
//            lat = 41;
//            lon = "-122.41";
//        };
//        dt = 1551639724;
//        id = 5562961;
//        main =     {
//            humidity = 87;
//            pressure = 1016;
//            temp = "281.57";
//            "temp_max" = "285.93";
//            "temp_min" = "278.15";
//        };
//        name = Dunsmuir;
//        rain =     {
//            1h = "0.25";
//        };
//        sys =     {
//            country = US;
//            id = 5150;
//            message = "0.005";
//            sunrise = 1551623977;
//            sunset = 1551665025;
//            type = 1;
//        };
//        visibility = 16093;
//        weather =     (
//                       {
//                           description = "light rain";
//                           icon = 10d;
//                           id = 500;
//                           main = Rain;
//                       }
//                       );
//        wind =     {
//            deg = "11.501";
//            speed = "1.26";
//        };
//    }

    self = [super init];
    if (self)
    {
     
        
        
        self.ID = (int)[dict objectForKey:@"weather"][0][@"id"];
        self.main =  [dict objectForKey:@"weather"][0][@"main"];
        self.desc =  [dict objectForKey:@"weather"][0][@"description"];
        self.iconID = [dict objectForKey:@"weather"][0][@"icon"];
        self.temprature = [[[dict valueForKey:@"main"] valueForKey:@"temp"] floatValue];
        self.country = [[dict valueForKey:@"sys"] valueForKey:@"country"];
        self.city = [dict valueForKey:@"name"];
    }
    NSLog(@"%@", [NSString stringWithFormat:@" weather Info  main %@ temp %f country %@ city %@",self.main,self.temprature,self.country,self.city]);
    return self;
}
@end
