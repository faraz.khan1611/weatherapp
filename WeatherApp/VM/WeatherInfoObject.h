//
//  WeatherInfoObject.h
//  WeatherApp
//
//  Created by Faraz Khan on 3/1/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
// THIS CLASS CONTAINS ALL DETAILS REQUIRED TO DRAW UI
//

#import <Foundation/Foundation.h>

@interface WeatherInfoObject : NSObject

@property(nonatomic) int ID;
@property(nonatomic) NSString* main;
@property(nonatomic) NSString* desc;
@property(nonatomic) NSString* iconID;
@property(nonatomic) float temprature;
@property(nonatomic) NSString* country;
@property(nonatomic) NSString* city;

-(WeatherInfoObject*)initWithDict:(NSDictionary*)dict;
@end
