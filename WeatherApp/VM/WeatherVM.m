//
//  WeatherVM.m
//  WeatherApp
//
//  Created by Faraz Khan on 3/1/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

#import "WeatherVM.h"

@interface WeatherVM()

@property(nonatomic, weak) id<WeatherVMDelegate> delegate;
@property(nonatomic) WController *controller;
@end

@implementation WeatherVM

-(WeatherVM*)init:(WController*)cntrl delegate:(id<WeatherVMDelegate>)delegate;
{
    self = [super init];
    if(self)
    {
        self.delegate = delegate;
        self.controller = cntrl;
        [self.controller addListener:self];
    }
    return self;
}

-(void)tearDown
{
    [self.controller cancelTimer];
    [self.controller removeListener:self];
}

-(WeatherInfoObject *)getCurrentWeatherInfo
{
    return [self.controller getCurrentWeatherInfo];
}

-(NSData *)getCurrentWeatherIcon
{
    return [self.controller getCurrentWeatherIcon];
}

-(LocationPermissionState)getCurrentLocationPermissionState
{
    return [self.controller getCurrentLocationPermissionState];
}

#pragma WController delegate
- (void)locationInfoError {
    if(self.delegate)
    {
        if([self.delegate respondsToSelector:@selector(locationInfoError)])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate locationInfoError];
            });
        }
    }
}

- (void)locationInfoUpdatedWithLat:(double)lat andLong:(double)longitute
{
    if(self.delegate)
    {
        if([self.delegate respondsToSelector:@selector(locationInfoUpdatedWithLat:andLong:)])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate locationInfoUpdatedWithLat:lat andLong:longitute];
            });
        }
    }
}

- (void)locationPermissionChanged:(LocationPermissionState)state
{
    if(self.delegate)
    {
        if([self.delegate respondsToSelector:@selector(locationPermissionChanged:)])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate locationPermissionChanged:state];
            });
        }
    }
}

- (void)weatherInfoError {
    if(self.delegate)
    {
        if([self.delegate respondsToSelector:@selector(weatherInfoError)])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate weatherInfoError];
            });
        }
    }
}

-(void) weatherInfoUpdatedWith:(WeatherInfoObject*)info{
    if(self.delegate)
    {
        if([self.delegate respondsToSelector:@selector(weatherInfoUpdated)])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate weatherInfoUpdated];
            });
        }
    }

}

-(void) weatherIconUpdated:(NSData*)data
{
    if(self.delegate)
    {
        if([self.delegate respondsToSelector:@selector(weatherIconUpdated:)])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate weatherIconUpdated:data];
            });
        }
    }
}

@end
