//
//  LocationInfoObject.m
//  WeatherApp
//
//  Created by Faraz Khan on 3/3/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

#import "LocationInfoObject.h"

@implementation LocationInfoObject
-(LocationInfoObject*)initWithLat:(double)lat main:(double)longitude
{
    self = [super init];
    if (self)
    {
        self.latitude = lat;
        self.longitude = longitude;
    }
    return self;
}
@end
