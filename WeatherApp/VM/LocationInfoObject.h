//
//  LocationInfoObject.h
//  WeatherApp
//
//  Created by Faraz Khan on 3/3/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocationInfoObject : NSObject
@property(nonatomic) double latitude;
@property(nonatomic) double longitude;

-(LocationInfoObject*)initWithLat:(double)lat main:(double)longitude;
@end
