//
//  ViewController.h
//  WeatherApp
//
//  Created by Faraz Khan on 3/1/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *CityLabel;
@property (weak, nonatomic) IBOutlet UILabel *TempratureLabel;
@property (weak, nonatomic) IBOutlet UIImageView *WeatherIconUIV;
@property (weak, nonatomic) IBOutlet UILabel *WeatherDescLabel;
@property (weak, nonatomic) IBOutlet UILabel *AlertLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *Loader;

@end

