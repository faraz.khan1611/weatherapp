//
//  ViewController.m
//  WeatherApp
//
//  Created by Faraz Khan on 3/1/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

#import "ViewController.h"
#import "WeatherVM.h"
#import "AppInteractor.h"

@interface ViewController ()<WeatherVMDelegate>
@property (nonatomic) WeatherVM *weatherVM;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.weatherVM = [[AppInteractor sharedInstance] getWeatherVm:self];
    [self prepareViewWithInfoObject];
    [self prepareIconForWeather];
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [self.weatherVM tearDown];
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)prepareViewWithInfoObject
{
    WeatherInfoObject *info = [self.weatherVM getCurrentWeatherInfo];
    
    if(info != nil)
    {
        [self showView];
        self.CityLabel.text = info.city;
        self.TempratureLabel.text =  [NSString stringWithFormat: @"%.0f°C", info.temprature - 273.15];
        self.WeatherDescLabel.text = info.desc;
    }
    else
    {
        [self hideView];
    }
}

-(void)prepareIconForWeather
{
    NSData *iconData = [self.weatherVM getCurrentWeatherIcon];
    if(iconData !=nil)
    {
        [self.WeatherIconUIV setHidden:NO];
        self.WeatherIconUIV.image = [UIImage imageWithData:iconData];
    }
    else
    {
        [self.WeatherIconUIV setHidden:YES];
    }
}

-(void)hideView
{
    [self.CityLabel setHidden:YES];
    [self.TempratureLabel setHidden:YES];
    [self.WeatherIconUIV setHidden:YES];
    [self.WeatherDescLabel setHidden:YES];
    [self.AlertLabel setHidden:YES];
    [self.Loader startAnimating];
}

-(void)showView
{
    [self.CityLabel setHidden:NO];
    [self.TempratureLabel setHidden:NO];
    [self.WeatherDescLabel setHidden:NO];
    [self.AlertLabel setHidden:YES];
    [self.Loader stopAnimating];
}


- (void)locationInfoError {
    [self.AlertLabel setHidden:NO];
}

- (void)locationInfoUpdatedWithLat:(double)lat andLong:(double)longitute {
    [self.AlertLabel setHidden:YES];
}

- (void)locationPermissionChanged:(LocationPermissionState)state {
    if(state != LocationPermissionStateLocationAuthorized)
    {
        [self.AlertLabel setHidden:NO];
    }
    else
    {
        [self.AlertLabel setHidden:YES];
    }
}

- (void)weatherInfoError {
    
}

- (void)weatherInfoUpdated {
    [self prepareViewWithInfoObject];
}

-(void) weatherIconUpdated:(NSData*)data
{
    [self prepareIconForWeather];
}
@end
