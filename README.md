This app fetches Weather using lat and long from device

The trigger points to update weather are:

1) Configurable timer, which can be configured using constants.h file.
2 ) location change beyond certain meter.
3) fresh launch


## Screenshot

![Webp.net-resizeimage__6_](/uploads/f7a607064d712bb2334b52bd61d548a5/Webp.net-resizeimage__6_.png)

![Webp.net-resizeimage__7_](/uploads/7d6d96a0e57faffa29d3b8107c10132b/Webp.net-resizeimage__7_.png)